OpEquals.java

Type
Java
Size
285 bytes
Storage used
285 bytes
Location
Level 2; Term 1
Owner
me
Modified
Jul 12, 2017 by me
Opened
Jul 12, 2017 by me
Created
Jul 12, 2017 with Google Drive Web
Add a description
Viewers can download
// Demonstrate several assignment operators.
class OpEquals {
public static void main(String args[]) {
int a = 1;
int b = 2;
int c = 3;
a += 5;
b *= 4;
c += a * b;
c %= 6;
System.out.println("a = " + a);
System.out.println("b = " + b);
System.out.println("c = " + c);
}
}